PCBNEW-LibModule-V1  7/6/2008-18:12:13
$INDEX
LGA14
$EndINDEX
$MODULE LGA14
Po 0 0 0 15 47990905 00000000 ~~
Li LGA14
Sc 00000000
Op 0 0 0
T0 0 0 600 600 0 120 N V 21 "LGA14"
T1 0 0 600 600 0 120 N V 21 "VAL**"
DS -984 590 984 590 19 21
DS 984 590 984 -590 19 21
DS 984 -590 -984 -590 19 21
DS -984 -590 -984 590 19 21
DC -787 433 -748 433 19 21
$PAD
Sh "1" R 236 354 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -787 393
$EndPAD
$PAD
Sh "2" R 236 354 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -472 393
$EndPAD
$PAD
Sh "3" R 236 354 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -157 393
$EndPAD
$PAD
Sh "4" R 236 354 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 157 393
$EndPAD
$PAD
Sh "5" R 236 354 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 472 393
$EndPAD
$PAD
Sh "6" R 236 354 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 787 393
$EndPAD
$PAD
Sh "7" R 236 354 0 0 900
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 787 0
$EndPAD
$PAD
Sh "8" R 236 354 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 787 -393
$EndPAD
$PAD
Sh "9" R 236 354 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 472 -393
$EndPAD
$PAD
Sh "10" R 236 354 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 157 -393
$EndPAD
$PAD
Sh "11" R 236 354 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -157 -393
$EndPAD
$PAD
Sh "12" R 236 354 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -472 -393
$EndPAD
$PAD
Sh "13" R 236 354 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -787 -393
$EndPAD
$PAD
Sh "14" R 236 354 0 0 2700
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -787 0
$EndPAD
$EndMODULE  LGA14
$EndLIBRARY
